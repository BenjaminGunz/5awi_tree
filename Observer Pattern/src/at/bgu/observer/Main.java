package at.bgu.observer;

public class Main {

	public static void main(String[] args) {
	
		
		
		Sensor s1 = new Sensor();
		Lantern l1 = new Lantern();
		Christbaum c1 = new Christbaum();
		Christbaum c2 = new Christbaum();
		TrafficLights t1 = new TrafficLights();
		TrafficLights t2 = new TrafficLights();
		s1.addObservable(c1);
		s1.addObservable(l1);
		s1.addObservable(c2);
		s1.addObservable(t2);
		s1.addObservable(t1);
		
		s1.informAll();
		
		
		
		
		
		
		
	}

}
