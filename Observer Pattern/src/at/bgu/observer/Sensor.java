package at.bgu.observer;

import java.util.ArrayList;
import java.util.List;

public class Sensor {

	private List<Observable> Observables;

	public Sensor() {
		super();
		this.Observables = new ArrayList<Observable>();

	}

	public void addObservable(Observable Obs) {
		this.Observables.add(Obs);
	}

	public void informAll() {
		for(Observable o : this.Observables) {
			o.inform();
		}
	}
}
