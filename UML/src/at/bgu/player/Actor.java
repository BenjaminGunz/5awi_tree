package at.bgu.player;

import java.util.ArrayList;
import java.util.List;


public class Actor {
	private String FirstName;
	private String LastName;
	private List<CD>CDs;
	private List<DVD>DVDs;
	public Actor(String firstName, String lastName, List<CD> cDs, List<DVD> dVDs) {
		super();
		this.FirstName = firstName;
		this.LastName = lastName;
		this.CDs = new ArrayList<CD>(cDs);
		this.DVDs = new ArrayList<DVD>(dVDs);
	}
	
	
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	
	public void addCD(CD CDs) {
		this.CDs.add(CDs);
	}
	
	public void addDVD(DVD DVDs) {
		this.DVDs.add(DVDs);
	}
	

}
