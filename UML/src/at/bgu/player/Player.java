package at.bgu.player;

import java.util.ArrayList;
import java.util.List;

public class Player {

	private String name;
	private List<Playable> Playables;

	public Player(String name, List<Actor> actors) {
		super();
		this.name = name;
		this.Playables = new ArrayList<Playable>(Playables);
	}

	public void addPlayable(Playable playables) {
		this.Playables.add(playables);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Player(String name) {
		super();
		this.name = name;
	}

	public void playSong() {

	}

	public void playVideo() {

	}

	public void play() {
		System.out.println("playing");
	}

	public void start() {
		System.out.println("started");
	}

	public void pause() {
		System.out.println("paused");
	}
}
