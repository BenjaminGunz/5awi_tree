package at.bgu.player;

public interface Playable {

	public void play();
	public void pause();
	public void start();
	
	
}
