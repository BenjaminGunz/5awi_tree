package at.bgu.treeschool;

import java.util.ArrayList;
import java.util.List;

public class ProductionArea {
	private List<Tree> trees;
	private String name;
	private int size;
	

	public ProductionArea(List<Tree> trees, String name, int size) {
		super();
		this.trees = new ArrayList<Tree>(trees);
		this.name = name;
		this.size = size;
	}
	
	public void fertilizeProcess() {
		
	}
	
	public void addTree(Tree tree) {
		this.trees.add(tree);
	}

	public List<Tree> getTrees() {
		return trees;
	}

	public void setTrees(List<Tree> trees) {
		this.trees = trees;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

}
