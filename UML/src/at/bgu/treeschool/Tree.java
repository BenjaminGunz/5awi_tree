package at.bgu.treeschool;

public abstract class Tree {
	private int maxSize;
	private int maxDiameter;
	private int fertilizeProcess;

	public Tree(int maxSize, int maxDiameter, int fertilizeProcess) {
		super();
		this.maxSize = maxSize;
		this.maxDiameter = maxDiameter;
		this.fertilizeProcess = fertilizeProcess;
	}

	public int getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}

	public int getMaxDiameter() {
		return maxDiameter;
	}

	public void setMaxDiameter(int maxDiameter) {
		this.maxDiameter = maxDiameter;
	}

	public int getFertilizeProcess() {
		return fertilizeProcess;
	}

	public void setFertilizeProcess(int fertilizeProcess) {
		this.fertilizeProcess = fertilizeProcess;
	}
	
	
}
