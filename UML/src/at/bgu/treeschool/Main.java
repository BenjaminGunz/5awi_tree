package at.bgu.treeschool;

public class Main {

	public static void main(String[] args) {
		Tree t1 = new Conifer(10,1,fp1);
		Tree t2 = new Leaftree(20,2,fp2);
		FertilizeProcess fp1 = new TopGrow;
		FertilizeProcess fp2 = new SuperGrow;
		
		ProductionArea pa = new ProductionArea(t1,"Doren",2000);
		pa.addTree(t2);
		pa.addTree(t1);
		
		pa.fertilizeProcess();
		
		

	}

}
