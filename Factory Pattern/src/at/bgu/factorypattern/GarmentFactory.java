package at.bgu.factorypattern;

public class GarmentFactory {
	 public static GarmentType createGarments(String selection) {
	        if (selection=="Trouser") {
	            return new Trouser();
	        } else if (selection=="Shirt") {
	            return new Shirt();
	        }
	        throw new IllegalArgumentException("Selection doesnot exist");
	    }
	  
}
