package at.bgu.factorypattern;


public class Client {

	public static void main(String[] args) {
        
      GarmentFactory.createGarments("Trouser").build();
       GarmentFactory.createGarments("Trouser").print();
       GarmentFactory.createGarments("Shirt").build();
       GarmentFactory.createGarments("Shirt").print();
    }
 
}
