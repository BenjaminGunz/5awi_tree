package at.bgu.factorypattern;

public class Trouser implements GarmentType{

	@Override
	public void print() {
		System.out.println("I am a Trouser");
		
	}

	@Override
	public void build() {
		System.out.println("Made a Trouser");
		
	}

}
