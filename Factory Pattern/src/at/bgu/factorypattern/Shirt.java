package at.bgu.factorypattern;

public class Shirt implements GarmentType{

	@Override
	public void print() {
		System.out.println("I am a Shirt");
		
	}

	@Override
	public void build() {
		System.out.println("Made a Shirt");
		
	}

}
