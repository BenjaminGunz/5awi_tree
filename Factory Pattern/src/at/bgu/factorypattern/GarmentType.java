package at.bgu.factorypattern;

public interface GarmentType {
	public void print();
	public void build();

}
